<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'homeController@create');

// Route::get('/business-template', function () {
//     return view('business-template');
// });

Route::get('/business-template', 'businestemplateController@create');

// Route::get('/about', function () {
//     return view('about');
// });

Route::get('/about', 'aboutController@create');

Route::get('/help', 'helpController@create');

// Route::get('/help', function () {
//     return view('help');
// });

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/dashboard', 'dashboardController@create');

// Route::get('/signup', function () {
//     return view('signup');
// });

Route::get('/signup', 'signupController@create');

// Route::get('/signin', function () {
//     return view('signin');
// });

Route::get('/login', 'loginController@create');


Route::post('/register', 'signupController@registerUser');
/*
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
