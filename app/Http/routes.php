<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'homeController@create');
Route::get('/business-template', 'businestemplateController@create');
Route::get('/about', 'aboutController@create');
Route::get('/help', 'helpController@create');


Route::get('auth/register', 'Auth\AuthController@getRegister');
	Route::post('auth/register', 'Auth\AuthController@postRegister');

	Route::get('auth/login', 'Auth\AuthController@getLogin');
	Route::post('auth/login', 'Auth\AuthController@postLogin');

	Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::group(array('before'=>'auth'), function(){
	Route::get('/dashboard', 'AdminController@dashboard');
	Route::get('/users', 'AdminController@users');
});



/*
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
