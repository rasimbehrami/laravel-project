<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Auth;

class signupController extends Controller
	{
		public function create()
		{
			return view('signup');
		}

		public function registerUser(Request $request)
		{
			$user = new User;
			$user->name = $request->input('name');
			$user->email = $request->input('email');
			$user->password = Hash::make($request->input('password'));
			$user->save();
			return view('home');
		}

		public function postLogin(Request $request)
		{
			$_email = $request->input('email');
			$_password = $request->input('password');

			if(Auth::attempt(['email' => $_email, 'password' => $_password])){
				//Authentication passed...
				return redirect()->intended('/dashboard');
			}
			else
			{
				//Authentication faild...
				//return '<script>alert("Authentication Failed");</script>';
				return view('home', ['message' => 'Authentication Failed']);
			}
		}
	}



?>