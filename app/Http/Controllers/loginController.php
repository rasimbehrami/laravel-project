<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class loginController extends Controller
	{
			public function getLogin()
			{
			
				return View('login');
			}

			public function postLogin(Request $request)
			{
				$data = [
				'username' => $request->input('username'),
				'password' => $request->input('password'),
				];	
			if (Auth::attempt($data, Input::get('remember')))
			{
				return Redirect::intended('/');
			}
			return Redirect::back()->with('error_message', 'Invalid data')->withInput();
			}

			public function getLogout()
			{
				$this->auth->logout();
				return redirect('/');
			}
	}



?>