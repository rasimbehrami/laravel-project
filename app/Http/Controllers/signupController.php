<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Auth;

class signupController extends Controller
	{
		public function create()
		{
			return view('signup');
		}

		public function registerUser(Request $request)
		{
			$user = new User;
			$user->name = $request->input('name');
			$user->email = $request->input('email');
			$user->password = Hash::make($request->input('password'));
			$user->save();
			return view('/');
		}

		public function postLogin(Request $request)
		{
		if ((Auth::attempt(['email' => $request->email, 'password' => $request->password]))
	
			)
		{
			return redirect()->intended('/admin');
		}
	}
}
?>
