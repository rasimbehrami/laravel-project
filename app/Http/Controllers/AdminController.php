<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
class AdminController extends Controller
	{

		 public function __construct()
    {
        $this->middleware('auth');
    }
    
		public function dashboard()
		{
			return view('admin.dashboard');
		}

		public function users() {

			$data = DB::table('users')->get();
			return view('admin.users')->with('data', $data);
	}
}


?>