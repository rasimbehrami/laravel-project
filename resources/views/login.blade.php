<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome to SurveyScribe</title>
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- FAVICON -->
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
    <!-- My Styles -->
    <link href="styles/main.css" rel="stylesheet">
  </head>
  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

  <header>
<div  id="content">
<div class="sign-up">
<img src="images/su-bg.jpg" id="su-bg"> 
<!-- su = sign up -->

              <h1>LOGIN user</h1>
              <h2><a href="/"><strong>Go Back</strong></a></h2>
            @if (count($errors) > 0)
               <div class="alert alert-danger">
               <srong>Whoops</srong>Something went wrong on your typing.<br><br>
                   <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                   </ul>
               </div>
             @endif

                <form action="{{ url('/login') }}" method="post">
                <div class="container-fluid">
                 <div class="row">
                   <div class="col-lg-4 col-lg-offset-4">
              
                          <div class="inner-addon">
                          <i class="glyphicon glyphicon-envelope"></i> 
                          <input type="email" class="form-control" id="su-email" name="email" placeholder="E-Mail"></div>
               
                          <div class="inner-addon">
                          <i class="glyphicon glyphicon-lock"></i>
                          <input type="password" class="form-control" id="su-password" name="password" placeholder="Password"></div>
                
                        <div class="row">
                         <div class="col-lg-6">
                           <div class="sign-up-btn pull-left">
                            <a class="btn btn-default" href="/" role="button">CANCEL</a></div>
                          </div>
                         <div class="col-lg-6"> 
                         <div class="sign-up-btn pull-right">
                           <button class="btn btn-default" type="submit">SIGN IN</button>
                         </div>
                      </div>
                    </div>
                   </div><!-- end col-lg-4 col-lg-offset-4 -->
                 </div><!-- end row -->
             </div>     <!-- end container-fluid -->
             </form><!-- end sing-up form -->
             </div><!-- end sign up -->
             </div><!-- end content -->

          <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- JS Smooth Scroll -->

  <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="js/jquery.plusanchor.js"></script>


<!-- Modal -->
              <form action="/login" method="post">
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">SIGN IN</h4>
                      </div>
                      <div class="modal-body">
                      <br>
                        <form class="form-group">
                          <div class="inner-addon-modal">
                            <i class="glyphicon glyphicon-envelope"></i> 
                          <input type="email" class="form-control" id="si-email" name="email" placeholder="E-Mail"></div>                          
                          <div class="inner-addon-modal">
                          <i class="glyphicon glyphicon-lock"></i>
                         <input type="password" class="form-control" id="si-password" name="password" placeholder="Password"></div>
                         <input type="checkbox" value="unit-in-group" />&nbsp; Keep me logged in
                      </form>                      
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-si-cancel" data-dismiss="modal">CANCEL</button>
                        <button class="btn btn-si-signin" type="submit">SIGN IN</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>





        