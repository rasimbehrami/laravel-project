<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome to SurveyScribe</title>
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- FAVICON -->
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
    <!-- My Styles -->
    <link href="styles/main.css" rel="stylesheet">

    <!-- Google Analytics -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-62380951-1', 'auto');
  ga('send', 'pageview');
</script>


  </head>
  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
 

  <header>

  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/home">SURVEY<span style="color:#78cee1">SCRIBE</span></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     

    <form class="navbar-form pull-right" role="search">
    <div class="input-group add-on">
      <input type="text" class="form-control" placeholder="Search for templates..." name="srch-term" id="srch-term">
      <div class="input-group-btn">
        <button class="btn btn-default" type="search"><i class="glyphicon glyphicon-search"></i></button>
      </div>
    </div>
  </form>
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="/">Home</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Browse Templates<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/business-template">Business</a></li>
            <li><a href="/browse-templates-political">Political</a></li>
            <li><a href="/browse-templates-application">Application</a></li>
          </ul>
        </li>
        <li><a href="/about">About Us</a></li>
        <li><a href="/help">Help</a></li>
      </ul>


    </div><!-- end navbar-collapse -->
  </div><!-- end container-fluid -->
</nav>

</header> <!-- end header -->

   <!-- Modal -->
                <form action="{{ url('/auth/register') }}" method="post">
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">SIGN IN</h4>
                      </div>
                      <div class="modal-body">
                      <br>
                        <form class="form-group">
                          <div class="inner-addon-modal">
                            <i class="glyphicon glyphicon-envelope"></i> 
                          <input type="email" class="form-control" id="si-email" name="email" placeholder="E-Mail"></div>
                          
                          <div class="inner-addon-modal">
                          <i class="glyphicon glyphicon-lock"></i>
                         <input type="password" class="form-control" name="password" id="si-password" placeholder="Password"></div>
                         <input type="checkbox" name="login" value="unit-in-group" />&nbsp; Keep me logged in
                      </form>
                      
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-si-cancel" data-dismiss="modal">CANCEL</button>
                        <button type="submit" class="btn btn-si-signin">SIGN IN</button>
                      </div>
                    </div>
                  </div>
                </div>
                </form><!-- end sign in form -->
  <body>
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/carousel_01.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
               <p class="slide-1-para"><strong>ONLINE FEEDBACK HAS NEVER BEEN EASIER</strong></p>
              <h1 class="slide-1-heading"><strong>SURVEY</strong></h1>
              <h2 class="slide-1-subheading"><strong>YOUR TARGET MARKET</strong></h2>
             
              <p><a class="btn btn-lg btn-primary" href="{{ url('/auth/register') }}" role="button">GET STARTED FOR FREE</a>
               <a class="btn btn-lg btn-default" href="#pricing" role="button">PLANS &amp PRICING</a></p>
               <br><br>
               <h5>Already a member? <a href="{{ url('/auth/login') }}" data-toggle="modal">Sign In</a></h5>
                

            </div><!--end carousel-caption-->
          </div><!--end container -->
        </div><!-- end item active -->
        <div class="item">
          <img src="images/carousel_02.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 class="slide-2-heading">Infinite Choices</h1>
              <p class="slide-2-para">Do more with our templates, edit and save in a few seconds.</p>
              <p><a class="btn btn-lg btn-primary" href="#featured" role="button">Featured Surveys</a>
             </p>
             </div><!--end carousel-caption-->
          </div><!--end container -->
        </div><!-- end item active -->
        <div class="item">
          <img src="images/carousel_03.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 class="slide-2-heading">Survey Support</h1>
              <p class="slide-2-para">Share instantly with the world.</p>
              <p><a class="btn btn-lg btn-primary" target="_blank" href="/help" role="button">Help Articles</a></p>
           </div><!--end carousel-caption-->
          </div><!--end container -->
        </div><!-- end item active -->
      </div><!--end carousel-ineer -->

      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div><!--end myCarousel -->


    <!-- SERVICES SECTION -->

    <section class="services">
    <div class="welcome-title">WELCOME TO SURVEY<span style="color:#78cee1">SCRIBE</span></div>
    <h6>A simple poll? In-depth market research? We’ve got you covered. 
    <br><br>Create any type of survey—from simple to sophisticated—with our powerful and easy-to-use survey builder! </h6>
    <div class="container" >
    <div class="row">
        
        <a href="#"><div class="col-sm-3">
        
          <div class="bg-form">
          <br>
          <div class="glyphicon glyphicon-list"></div> 
          <div class="service-title">10 QUESTION TYPES</div> 
          <div class="service-info">Choose what best fits for you and <br> you’re ready to go! </div> 

          <br>
          </div>
          </div> </a>


           <a href="#"><div class="col-sm-3">
        
          <div class="bg-form">
          <br>
          <div class="glyphicon glyphicon-cog"></div> 
            <div class="service-title">CUSTOM BRANDING</div>
            <div class="service-info"> Personalize surveys with your <br> company logo, slogan and description!</div>

          <br>
          </div>
          </div> </a>


          <a href="#"><div class="col-sm-3">
        
          <div class="bg-form">
          <br>
          <div class="glyphicon glyphicon-tasks"></div>
            <div class="service-title">REAL TIME RESULTS</div>
            <div class="service-info">Just by the time users submit surveys, <br> the creator can see answers.</div>

          <br>
          </div>
          </div> </a>

          <a href="#"><div class="col-sm-3">
        
          <div class="bg-form">
          <br>
             <div class="glyphicon glyphicon-globe"></div>
            <div class="service-title">WORLDWIDE</div>
            <div class="service-info">Reach your audience worldwide! <br> Or filter it!</div>

          <br>
          </div>
          </div> </a>


        </div><!-- end row -->
        </div><!-- end container -->

        </section><!-- end services -->


        <!-- SERVICES DESCRIPTION SECTION -->


        <section class="service-description">
          <div class="container-fluid">
            <div class="container">
              <div class="row">
              <div class="col-md-6">
                <h1>10 QUESTION TYPES</h1>
                <h4>Interesting surveys come with different question types like checkboxes, ticks ect. <br>
                    This makes it easier to respondents to deal with long surveys and different info. <br><br>
                    Learn more how you can <a href="browse-templates-political-edit2.html"><span class="sd-link">  edit your own survey template now!</span></a></h4>
              </div> <!-- end col-md-6 -->
              <div class="col-md-6">
              <div class="glyphicon glyphicon-list-alt"></div>
              </div> <!-- end col-md-6 -->
            </div><!-- end row -->
            </div><!-- end container -->
            </div><!-- end container-fluid -->
        </section> <!-- end Service Description section -->

          <!-- PRICING PLANS SECTION -->

         <section class="pricing-plans" id="pricing">
           
           <div class="container-fluid">
                <h1>PLANS &amp PRICING</h1>
                <h2>xxxxx</h2>

            </div> <!-- end container-fluid -->
              
              <div class="container">
              <div class="row">
              <div class="col-md-4">
                <div class="box">
                   <div class="box-title">
                  <div class="plan-name">BASIC</div>
                  <div class="plan-customer">For Individuals</div>
                </div>
                 <div class="box-subtitle" id="gradient-1">
                  <div class="plan-price">FREE</div>
                </div>
                 <div class="box-features">
                  <div class="features-text"><strong>up to 1000 </strong>respondents
                                         <br><br><strong>up to 50 </strong> questions/month
                                        <br><br><strong>50 </strong> surveys/month
                  </div><!-- end features-text -->
                  <div class="button-1"><a href="{{ url('/auth/register') }}"><button type="button" class="btn btn-default">Sign Up</button></a></div>
                </div><!-- end box-features -->
              </div><!-- end box -->
              </div><!-- end col-md-4 -->
              <div class="col-md-4">
                 <div class="box">
                   <div class="box-title">
                  <div class="plan-name">PRO</div>
                  <div class="plan-customer">For Professional Teams</div>
                </div>
                 <div class="box-subtitle" id="gradient-2">
                  <div class="plan-price"><span class="glyphicon glyphicon-usd"></span>24 </span><font size="3">per month</font></div></div>

                 <div class="box-features">
                  <div class="features-text"><strong>up to 10 000 </strong>respondents
                                         <br><br><strong>up to 100 </strong> questions/month
                                        <br><br><strong>150 </strong> surveys/month
                  </div><!-- end features-text -->
                  <div class="button-2"><a href="/pricing"><button type="button" class="btn btn-default">Choose Plan</button></a></div>
                </div><!-- end box-features -->
              </div><!-- end box -->
              </div><!-- end col-md-4 -->
              <div class="col-md-4">
                 <div class="box">
                   <div class="box-title">
                  <div class="plan-name">BUSINESS</div>
                  <div class="plan-customer">For Big Companies</div>
                </div>
                 <div class="box-subtitle" id="gradient-3">
                  <div class="plan-price"><span class="glyphicon glyphicon-usd "></span>100 <font size="3">per month</font></div>
                </div>
                 <div class="box-features">
                  <div class="features-text"><strong>Unlimited </strong>respondents
                                         <br><br><strong>Unlimited </strong> questions/month
                                        <br><br><strong>Unlimited </strong> surveys/month
                  </div><!-- end features-text -->
                  <div class="button-1"><a href="/pricing"><button type="button" class="btn btn-default">Choose Plan</button></a></div>
                </div><!-- end box-features -->
              </div><!-- end box -->
              </div><!-- end col-md-4 -->

           </div><!-- end row -->
           
         </div><!-- end container -->

         </section> <!-- end Pricing Plans section -->
          
          <!-- FEATURED SURVEYS SECTION -->

        <section class="featured-surveys" id="featured">
          <div class="container-fluid">
                <h1>FEATURED SURVEYS</h1>
                <h2>xxxxx</h2>
              
               <div id="navcontainer">
              <ul id="navlist">
              <li class="active"><a href="#" id="current">SHOW ALL</a></li>
              <li><a href="/browse-templates-business">BUSINESS</a></li>
              <li><a href="/browse-templates-political">POLITICAL</a></li>
              <li><a href="/browse-templates-application">APPLICATION</a></li>
              </ul>
              </div><!-- end nav-container -->

              <div class="col-wrapper-1">
                
              <div class="col-md-3">

                 <a href="#"><div class="rectangular-1"></div></a>

              </div>
              
              <div class="col-md-3 ">
                 
                  <a href="#"><div class="rectangular-2"></div></a>
                
              </div>

              <div class="col-md-3 ">

                 <a href="#"><div class="rectangular-1"></div></a>

              </div>

               <div class="col-md-3 ">

                 <a href="#"><div class="rectangular-2"></div></a>

              </div>

              </div><!-- end col-wrapper-1-->

                <div class="col-wrapper-2">
                
              <div class="col-md-3">

                <a href="#"><div class="rectangular-1"></div></a>

              </div>
              
              <div class="col-md-3 ">

                 <a href="#"><div class="rectangular-2"></div></a>

              </div>

              <div class="col-md-3 ">

                 <a href="#"><div class="rectangular-1"></div></a>

              </div>

               <div class="col-md-3 ">

                 <a href="#"><div class="rectangular-2"></div></a>

              </div>

              </div><!-- end col-wrapper-2 -->

              <div class="show-more-btn">
              <button type="button" class="btn btn-default">LOAD MORE</button>
              </div> <!-- end show-more-btn -->

              </div><!-- end container-fluid-->

        </section><!-- end section featured-surveys-->


         <!-- CONTACT FORM SECTION -->

        <section class="contact-form">
          
          <div class="container">
          <h1>GET IN TOUCH</h1>
           

           <form class="contact-form">
             
           <div class="container">
             <div class="row">

               <div class="col-sm-4">
                 <input type="name" class="form-control" placeholder="Name (required)">
               </div>

               <div class="col-sm-4">
                 <input type="email" class="form-control" placeholder="E-Mail (required)">
               </div>

               <div class="col-sm-4">
                 <input type="subject" class="form-control" placeholder="Subject (optional)">
               </div>

             </div> <!-- end row -->

             <div class="row">
               <div class="message"><textarea rows="5" style="max-height:8em;"class="form-control" id="message" placeholder="Message (required)"></textarea></div>
             </div><!-- end row -->

           </div><!-- end container -->
              <div class="send-message-btn">
              <a class="btn btn-default" href="#" role="button">SEND MESSAGE</a>
            </div><!-- end send-message-btn-->

           </form> <!-- end contact-form -->
          </div><!-- end container -->
         
        </section><!-- end section contact-form -->


  </body> <!-- end body -->

         <!-- FOOTER -->

          <footer class="footer">
        

          <div class="container-fluid">

            <div id="navcontainer">

           <ul id="navlist">
            <li class="fade"><a href="/sitemap">Sitemap</a></li>
            <li class="fade"><a href="/privacy">Privacy</a></li>
            <li class="fade"><a href="http://surveyscribe.blogspot.com/">Blog</a></li>
           </ul><!-- end navlist -->

          </div> <!-- end navcontainer -->
          
          <div class="social-media">
            <a target="_blank" class="icon fade" href="https://www.facebook.com"><img src="images/fb-logo.png"></a>
            <a target="_blank" class="icon fade" href="https://www.twitter.com"><img src="images/twitter-logo.png"></a>
            <a target="_blank" class="icon fade"href="https://www.youtube.com"><img src="images/youtube-logo.png"></a>
          </div>  <!-- end social-media -->
          </div> <!-- end container-fluid -->

          <div class="copyright-text">
            <p style="text-align: center">SURVEY SCRIBE 2015 | ALL RIGHTS RESERVED</p>
          </div> <!-- end copyright-text -->

        </footer> <!-- end footer -->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- JS Smooth Scroll -->

  <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="js/jquery.plusanchor.js"></script>
  

  <script type="text/javascript">
    jQuery(document).ready(function($){
      $('body').plusAnchor({
        easing: 'easeInOutExpo',
        offsetTop: -20,
        speed: 1000,
        onInit: function( base ) {

          if ( base.initHash != '' && $(base.initHash).length > 0 ) {
            window.location.hash = 'hash_' + base.initHash.substring(1);
            window.scrollTo(0, 0);

            $(window).load( function() {

              timer = setTimeout(function() {
                $(base.scrollEl).animate({
                  scrollTop: $( base.initHash ).offset().top
                }, base.options.speed, base.options.easing);
              }, 2000); // setTimeout

            }); // window.load
          }; // if window.location.hash
        } // onInit
      });
    });
  </script>
  <!-- END JS Smooth Scroll -->


</html>