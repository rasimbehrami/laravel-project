
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome to SurveyScribe</title>
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- FAVICON -->
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
    <!-- My Styles -->
    <link href="styles/main.css" rel="stylesheet">

    <!-- Google Analytics -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-62380951-1', 'auto');
      ga('send', 'pageview');
    </script>
  </head>


  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

  <header>

  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/home">SURVEY<span style="color:#78cee1">SCRIBE</span></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     

    <form class="navbar-form pull-right" role="search">
    <div class="input-group add-on">
      <input type="text" class="form-control" placeholder="Search for templates..." name="srch-term" id="srch-term">
      <div class="input-group-btn">
        <button class="btn btn-default" type="search"><i class="glyphicon glyphicon-search"></i></button>
      </div>
    </div>
  </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/">Home</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Browse Templates<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/business-template">Business</a></li>
            <li><a href="/browse-templates-political">Political</a></li>
            <li><a href="/browse-templates-application">Application</a></li>
          </ul>
        </li>
        <li><a href="/about">About Us</a></li>
        <li class="active"><a href="/help">Help</a></li>
      </ul>


    </div><!-- end navbar-collapse -->
  </div><!-- end container-fluid -->
</nav>

</header> <!-- end header -->
<title>Help Center - SurveyScribe</title>

        <div id="content">
        <div class="help">
              <div class="container">
                <h1><strong>HELP CENTER</strong></h1>
                <h2>Click any of the categories to begin!</h2>
                 <div class="row">
                   
                 <div class="col-md-6">
                    <div id="accordion" class="list-group">
                     <a class="list-group-item" data-toggle="collapse" data-parent="#accordion" data-target="#listofOptions">Getting Started <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
                    <div class="list-group-collapse collapse" id="listofOptions">
                      <p class="getting-started"><br>Welcome to Anketa.
                      The Help Center is under construction. Please come back later.
                      We are working on adding a lot of useful articles here
                      printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and <br></p>
                   </div>
                   <br>
                   <br>
                   <a class="list-group-item" data-toggle="collapse" data-parent="#accordion" data-target="#listofOptions2">Edit Survey Templates<span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
                   <div class="list-group-collapse collapse" id="listofOptions2">
                   <p class="build-survey"><br><br>Editing a survey is very easy with Anketa. <br>Here is a walkthrough to this process. <br><br> 1. Go to the navbar and click Browse Templates <br> 2. Choose your desired category <br> 3. From the gallery of the templates, choose the one that best fits you. <br> 4. Next to each question use the tools provided to edit the template. <br> - The pencil tool enables you to change he text of the question<br> &nbsp; &nbsp; or the number of choices given to the answer. <br> - The second tool rearranges questions in a new order <br> - The third one removes a particular question from the template. <br> 5. In the right page, give your survey a title and optionally <br>&nbsp; &nbsp; a description and an image and you are ready to go. <br> <br> 
                   For further information look at the video demo below:
                   <br></p>
                  <iframe width="520" height="295" src="https://www.youtube.com/embed/e--ZgOuQo1w" frameborder="0" allowfullscreen></iframe>
                 </div>   
                  <br>
                 <br>  
                 <a class="list-group-item" data-toggle="collapse" data-parent="#accordion" data-target="#listofOptions3">Sharing Surveys <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
                   <div class="list-group-collapse collapse" id="listofOptions3">
                   <p class="build-survey"><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum<br></p>
                 </div>
                 
                 </div>
                 </div>

                 <div class="col-md-6">
                    <div id="accordion" class="list-group">
                     <a class="list-group-item" data-toggle="collapse" data-parent="#accordion" data-target="#listofOptions4">Analysing Results <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
                    <div class="list-group-collapse collapse" id="listofOptions4">
                      <p class="getting-started"><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>
                   </div>
                   <br>
                   <br>
                   <a class="list-group-item" data-toggle="collapse" data-parent="#accordion" data-target="#listofOptions5">Purchasing a Plan  <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
                   <div class="list-group-collapse collapse" id="listofOptions5">
                   <p class="build-survey"><br>We offer various packages in order to fulfill our client's needs. <br> The packages that we offer are: Basic, Pro and Business. <br> As the Basic package is free 
                    you just have to Sign Up/Sign In to get it. <br>
                    The packages Pro and Business are not free so you have to pay for them in order to use them. To make the buying process more easier we help you by showing a little guide on how to buy these packages. 
                    The steps that you have to follow are listed below: <br><br>
                    1. You have to Sign in to your registered profile or if you haven't created a profile already you can Sign Up. <br> 
                    2. After signing in go to the section Plans &amp Pricing at our homepage. <br>
                    3. Choose the package that best fits you by clicking on it. <br>
                    4. After clicking the package, you are moved to another page where you have to complete a form to buy the package. The form consists of: <br>
                   a) Payment method - You can make the payment on Paypal or by using a credit card <br>
                   b) Billing details- You have to fulfill a form that requires your email, country, address, phone number, city etc. <br>
                  5. After completing all the details explained above at the end of the page appears a summary of the order you're making. <br>
                  6. If you're sure that you haven't done any mistake you can go ahead buying your package by clicking confirm. <br>
                  7. After clicking confirm appears a message that the package was successfully added to cart and that's it, you're done, simple as that. <br> <br>
                  For further information look at the video demo below: <br>
                    <br></p>

                    <!-- VENDOS KODIN EMBED TE VIDEOS SE YOUTUBE KETU -->
                    
                 </div>   
                 <br>
                 <br>
                  <a class="list-group-item" data-toggle="collapse" data-parent="#accordion" data-target="#listofOptions6">Managing your Account <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
                   <div class="list-group-collapse collapse" id="listofOptions6">
                   <p class="build-survey"><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>
                 </div>     
                 </div>
                 </div>


                 </div>

              </div><!-- end container -->
              </div><!-- end help -->
        </div><!-- end content -->


         <!-- FOOTER -->

          <footer class="footer">
        

          <div class="container-fluid">

            <div id="navcontainer">

           <ul id="navlist">
            <li class="fade"><a href="/sitemap">Sitemap</a></li>
            <li class="fade"><a href="/privacy">Privacy</a></li>
            <li class="fade"><a href="http://surveyscribe.blogspot.com/">Blog</a></li>
           </ul><!-- end navlist -->

          </div> <!-- end navcontainer -->
          
          <div class="social-media">
            <a target="_blank" class="icon fade" href="https://www.facebook.com"><img src="images/fb-logo.png"></a>
            <a target="_blank" class="icon fade" href="https://www.twitter.com"><img src="images/twitter-logo.png"></a>
            <a target="_blank" class="icon fade"href="https://www.youtube.com"><img src="images/youtube-logo.png"></a>
          </div>  <!-- end social-media -->
          </div> <!-- end container-fluid -->

          <div class="copyright-text">
            <p style="text-align: center">SURVEY SCRIBE 2015 | ALL RIGHTS RESERVED</p>
          </div> <!-- end copyright-text -->

        </footer> <!-- end footer -->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- JS Smooth Scroll -->

  <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="js/jquery.plusanchor.js"></script>
  

  <script type="text/javascript">
    jQuery(document).ready(function($){
      $('body').plusAnchor({
        easing: 'easeInOutExpo',
        offsetTop: -20,
        speed: 1000,
        onInit: function( base ) {

          if ( base.initHash != '' && $(base.initHash).length > 0 ) {
            window.location.hash = 'hash_' + base.initHash.substring(1);
            window.scrollTo(0, 0);

            $(window).load( function() {

              timer = setTimeout(function() {
                $(base.scrollEl).animate({
                  scrollTop: $( base.initHash ).offset().top
                }, base.options.speed, base.options.easing);
              }, 2000); // setTimeout

            }); // window.load
          }; // if window.location.hash
        } // onInit
      });
    });
  </script>
  <!-- END JS Smooth Scroll -->


</html>