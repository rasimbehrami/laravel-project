<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome to SurveyScribe</title>
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- FAVICON -->
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
    <!-- My Styles -->
    <link href="styles/main.css" rel="stylesheet">

    <!-- Google Analytics -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-62380951-1', 'auto');
  ga('send', 'pageview');
</script>


  </head>
  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
 

  <header>

  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/home">SURVEY<span style="color:#78cee1">SCRIBE</span></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     

    <form class="navbar-form pull-right" role="search">
    <div class="input-group add-on">
      <input type="text" class="form-control" placeholder="Search for templates..." name="srch-term" id="srch-term">
      <div class="input-group-btn">
        <button class="btn btn-default" type="search"><i class="glyphicon glyphicon-search"></i></button>
      </div>
    </div>
  </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/">Home</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Browse Templates<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/business-template">Business</a></li>
            <li><a href="/browse-templates-political">Political</a></li>
            <li><a href="/browse-templates-application">Application</a></li>
          </ul>
        </li>
        <li class="active"><a href="/about">About Us</a></li>
        <li><a href="/help">Help</a></li>
      </ul>


    </div><!-- end navbar-collapse -->
  </div><!-- end container-fluid -->
</nav>

</header> <!-- end header -->


        

        <div id="content">
        <div class="about-us">
        <div class="about-header">
          <div class="about-header-title">ABOUT US</div>
          <div class="about-header-subtitle">Our goal is to help individuals and companies to get feedback in the fastest and easiest way<br>for every subject they’re interested in !</div>
        </div><!-- end about-header -->
  
          <div class="about-subheader">
          <div class="container">
              <p class="text1">SurveyScribe, launched in 2015 is a survey company that offers the best tools to help you create your own survey. The company is established by a group of  passionate engineers who work hard to give you the best services and products. 
               It’s very important for us to have satisfied clients that’s why we work continuously to improve our sevices and we always
                get feedback from our clients.</p>

              <p class="text2">Our service is simple, we help you to choose from a variety of templates created  by experts so that you can create your survey faster and easier.  Also you have the option to build your own survey from scratch.  The service  gives real-time results and you can get feedback starting from the moment that you’ve created your survey. For every problem you might encounter we’re 
              there to offer our  24/7 support.</p>
        </div><!-- end container-->
        </div><!-- end about-subheader -->
        <div class="about-whyus">
            <h1>WHY CHOOSE US?</h1>
            <h2>xxxxx</h2>
        <div class="container">
        <h3 class="about-whyus-subtitle">We want satisfied clients for the work that we’re doing and we believe that we can achieve this for the main reasons below:</h3>
        <div class="col-md-6 whyus-lists">
         <ul>
              <li>Individuals can use every basic function free of charge </li>
              <li>Enterprise companies benefit the most out of our system</li>
              <li>All customers enjoy 24h support 7 days a week.</li>
         </ul>
        </div><!-- end col-md-6 -->

        <div class="col-md-6 whyus-lists">
         <ul>
              <li>The system keeps improving everyday with constant feedback.</li>
              <li>Your respondents data is encrypted using SSL 256-AES Encrytion.</li>
              <li>Features to come include creating a new survey from scratch.</li>
         </ul>
        </div><!-- end col-md-6 -->
        </div><!-- end container -->
        </div><!-- end about-whyus-->
        <div class="about-contacts">
          <h1>CONTACTS</h1>
          <h2>xxxxx</h2>
          <div class="container">
            <div class="col-md-4">
          
              <div class="glyphicon glyphicon-map-marker"></div>
            <div class="service-title">HEAD OFFICE</div>
            <div class="service-info">Street. Myslym Shyri <br> Tirana, Albania</div>
      
        </div>  <!-- end col-sm-3 -->

        <div class="col-md-4">
            
             <div class="glyphicon glyphicon-earphone"></div> 
            <div class="service-title">CALL US</div>
            <div class="service-info"> Phone: +xxxxxxxxxx <br> Fax: +xxxxxxxxxxxx</div>
      
        </div> <!-- end col-sm-3 -->

        <div class="col-md-4">
     
             <div class="glyphicon glyphicon-globe"></div>
            <div class="service-title">EMAIL</div>
            <div class="service-info"> info@anketa.al <br> support@anketa.al</div>
     
        </div>  <!-- end col-sm-3 -->
        </div><!-- end container -->   
        </div><!-- end about-contacts-->

        <div class="about-map">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d47935.728607281264!2d19.817823200000003!3d41.330982199999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1350310470fac5db%3A0x40092af10653720!2sTirana%2C+Albania!5e0!3m2!1sen!2s!4v1427325355434" width="100%" height="422" frameborder="0" style="border:0"></iframe>
        </div><!-- end map -->

        <div class="about-team">

<div class="container-fluid">
                <h1>OUR CREATIVE TEAM</h1>
                <h2>xxxxx</h2>
            </div>

<h4>Our team is a group of qualified engineers who work hard to achieve their goals. We offer full support for our clients and 
  <br>we are always working. </h4>

  <div class="container">
<div class="row">
<div class="col-md-6">
      <div class="pull-right">
       <div class="col-md-6">
                <div class="member-wrapper">
                <div class="col-md-6">
                  <div class="member-name">GEZIM MUSLIAJ</div>
                  <div class="member-prof">Software Engineer</div>
                  <div class="member-desc">He has studied Computer 
                                         <br>Engineering at Polytechnic 
                                        <br>University of Tirana.
                  </div>
              <div class="contacts-media">
             <a class="icon" target="_blank" href="#"><i class="fa fa-twitter"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-behance"></i></a>
            <a class="icon" target="_blank" href="https://www.facebook.com/gmusliaj"><i class="fa fa-facebook"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-instagram"></i></a>
          </div>
                </div><!-- end col-md-6 -->

                <div class="col-md-6">
                <img src="images/gezim.jpg">
                </div><!-- end col-md-6 -->
                </div><!-- end member-wrapper -->
                </div><!-- end col-md-6 -->
                </div><!-- end pull-right -->
                </div><!-- end main col-md-6-->


<div class="col-md-6">

       <div class="col-md-6">
                <div class="member-wrapper">
                <div class="col-md-6">
                  <div class="member-name">PAOLA LERA</div>
                  <div class="member-prof">Software Developer</div>
                  <div class="member-desc">She has studied Electronic 
                                         <br>Engineering at Polytechnic 
                                        <br>University of Tirana.
                  </div>
              <div class="contacts-media">
            <a class="icon" target="_blank" href="#"><i class="fa fa-twitter"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-behance"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-facebook"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-instagram"></i></a>
          </div>
                </div><!-- end col-md-6 -->

                <div class="col-md-6">
                <img src="images/paola.jpg">
                </div><!-- end col-md-6 -->
                </div><!-- end member-wrapper -->
                </div><!-- end col-md-6 -->
                </div><!-- end main col-md-6-->
</div><!-- end first row -->

<div class="row">
<div class="col-md-6">
      <div class="pull-right">
       <div class="col-md-6">
                <div class="member-wrapper">
                <div class="col-md-6">
                  <div class="member-name">FABIO ÇIMO</div>
                  <div class="member-prof">Web Designer/Developer</div>
                  <div class="member-desc">He studies Computer 
                                         <br>Engineering at Polytechnic 
                                        <br>University of Tirana.
                  </div>
              <div class="contacts-media">
            <a class="icon" target="_blank" href="https://www.twitter.com/fabiocimo"><i class="fa fa-twitter"></i></a>
            <a class="icon" target="_blank" href="https://www.behance.com/fabiocimo"><i class="fa fa-behance"></i></a>
            <a class="icon" target="_blank" href="https://www.facebook.com/fabiocimo"><i class="fa fa-facebook"></i></a>
            <a class="icon" target="_blank" href="https://www.instagram.com/fabiocimo"><i class="fa fa-instagram"></i></a>
          </div>
                </div><!-- end col-md-6 -->

                <div class="col-md-6">
                <img src="images/fabio.jpg">
                </div><!-- end col-md-6 -->
                </div><!-- end member-wrapper -->
                </div><!-- end col-md-6 -->
                </div><!-- end pull-right -->
                </div><!-- end main col-md-6-->



<div class="col-md-6">

       <div class="col-md-6">
                <div class="member-wrapper">
                <div class="col-md-6">
                  <div class="member-name">ALBI DEMA</div>
                  <div class="member-prof">Software Engineer</div>
                  <div class="member-desc">He studies Computer
                                         <br>Engineering at Polytechnic 
                                        <br>University of Tirana.
                  </div>
              <div class="contacts-media">
            <a class="icon" target="_blank" href="#"><i class="fa fa-twitter"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-behance"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-facebook"></i></a>
            <a class="icon" target="_blank" href="#"><i class="fa fa-instagram"></i></a>
          </div>
                </div><!-- end col-md-6 -->

                <div class="col-md-6">
                <img src="images/albi.jpg">
                </div><!-- end col-md-6 -->
                </div><!-- end member-wrapper -->
                </div><!-- end col-md-6 -->
                </div><!-- end main col-md-6-->
</div><!-- end second row -->
       </div><!-- end container -->

        </div><!-- end about-team -->


        </div><!-- end about-us -->
        </div><!-- end content -->




         <!-- FOOTER -->

          <footer class="footer">
        

          <div class="container-fluid">

            <div id="navcontainer">

           <ul id="navlist">
            <li class="fade"><a href="/sitemap">Sitemap</a></li>
            <li class="fade"><a href="/privacy">Privacy</a></li>
            <li class="fade"><a href="http://surveyscribe.blogspot.com/">Blog</a></li>
           </ul><!-- end navlist -->

          </div> <!-- end navcontainer -->
          
          <div class="social-media">
            <a target="_blank" class="icon fade" href="https://www.facebook.com"><img src="images/fb-logo.png"></a>
            <a target="_blank" class="icon fade" href="https://www.twitter.com"><img src="images/twitter-logo.png"></a>
            <a target="_blank" class="icon fade"href="https://www.youtube.com"><img src="images/youtube-logo.png"></a>
          </div>  <!-- end social-media -->
          </div> <!-- end container-fluid -->

          <div class="copyright-text">
            <p style="text-align: center">SURVEY SCRIBE 2015 | ALL RIGHTS RESERVED</p>
          </div> <!-- end copyright-text -->

        </footer> <!-- end footer -->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- JS Smooth Scroll -->

  <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="js/jquery.plusanchor.js"></script>
  

  <script type="text/javascript">
    jQuery(document).ready(function($){
      $('body').plusAnchor({
        easing: 'easeInOutExpo',
        offsetTop: -20,
        speed: 1000,
        onInit: function( base ) {

          if ( base.initHash != '' && $(base.initHash).length > 0 ) {
            window.location.hash = 'hash_' + base.initHash.substring(1);
            window.scrollTo(0, 0);

            $(window).load( function() {

              timer = setTimeout(function() {
                $(base.scrollEl).animate({
                  scrollTop: $( base.initHash ).offset().top
                }, base.options.speed, base.options.easing);
              }, 2000); // setTimeout

            }); // window.load
          }; // if window.location.hash
        } // onInit
      });
    });
  </script>
  <!-- END JS Smooth Scroll -->


</html>