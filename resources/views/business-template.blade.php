<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome to SurveyScribe</title>
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- FAVICON -->
    <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
    <!-- My Styles -->
    <link href="styles/main.css" rel="stylesheet">

    <!-- Google Analytics -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-62380951-1', 'auto');
      ga('send', 'pageview');
    </script>
  </head>


  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

  <header>

  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/home">SURVEY<span style="color:#78cee1">SCRIBE</span></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     

    <form class="navbar-form pull-right" role="search">
    <div class="input-group add-on">
      <input type="text" class="form-control" placeholder="Search for templates..." name="srch-term" id="srch-term">
      <div class="input-group-btn">
        <button class="btn btn-default" type="search"><i class="glyphicon glyphicon-search"></i></button>
      </div>
    </div>
  </form>
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="/">Home</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Browse Templates<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/business-template">Business</a></li>
            <li><a href="/browse-templates-political">Political</a></li>
            <li><a href="/browse-templates-application">Application</a></li>
          </ul>
        </li>
        <li><a href="/about">About Us</a></li>
        <li><a href="/help">Help</a></li>
      </ul>


    </div><!-- end navbar-collapse -->
  </div><!-- end container-fluid -->
</nav>

</header> <!-- end header -->


  <div id="content" class="templates-wrapper">
    
          
      <div class="container">

      <h1>Business Templates</h1>
      <h5>Browse our professional templates on business topics.</h5>
      <div class="row">
        
       <a href="/coming-soon"> <div class="col-md-4">
        
        <div class="bg-form">
        <h2> SALES INDEX </h2>
        <h3> Useful to get info on the number of sales.</h3>
        <img src="images/sb1.jpg">
        <h5><span class="glyphicon glyphicon-pencil"></span>&nbsp; &nbsp; &nbsp; Full Editing Capabilities</h5>
        <h5><span class="glyphicon glyphicon-list"></span>&nbsp; &nbsp; &nbsp; 8 Question Types</h5>
        <h5><span class="glyphicon glyphicon-fire"></span>&nbsp; &nbsp; &nbsp; Trending Template</h5>
      </div><!-- end bg-form -->

      </div><!-- end col-md-4 -->
      </a>

        <a href="/coming-soon"> <div class="col-md-4">
        <div class="bg-form">
        <h2> MARKET RESEARCH </h2>
        <h3> Useful for information on the market.</h3>
        <img src="images/sb2.jpg">
        <h5><span class="glyphicon glyphicon-wrench"></span>&nbsp; &nbsp; &nbsp; Partial Editing Capabilities</h5>
        <h5><span class="glyphicon glyphicon-tasks"></span>&nbsp; &nbsp; &nbsp; 5 Question Types</h5>
        <h5><span class="glyphicon glyphicon-flash"></span>&nbsp; &nbsp; &nbsp; Featured Template</h5>

      </div><!-- end bg-form -->
      </div><!-- end col-md-4 -->
      </a>

        <a href="/coming-soon"> <div class="col-md-4">
        <div class="bg-form">
        <h2> PRODUCT USABILITY </h2>
        <h3> Useful to get product feedback.</h3>
        <img src="images/sb3.jpg">
        <h5><span class="glyphicon glyphicon-saved"></span>&nbsp; &nbsp; &nbsp; Ready to Use</h5>
        <h5><span class="glyphicon glyphicon-list"></span>&nbsp; &nbsp; &nbsp; 10 Question Types</h5>
        <h5><span class="glyphicon glyphicon-pushpin"></span>&nbsp; &nbsp; &nbsp; Worldwide Audience</h5>

      </div><!-- end bg-form -->
      </div><!-- end col-md-4 -->
      </a>
      </div><!-- end row-->
      </div><!-- end container -->


  </div> <!-- end content -->
         <!-- FOOTER -->

        <footer class="footer"> 
          <div class="container-fluid">
            <div id="navcontainer">
               <ul id="navlist">
                <li class="fade"><a href="/sitemap">Sitemap</a></li>
                <li class="fade"><a href="/privacy">Privacy</a></li>
                <li class="fade"><a href="http://surveyscribe.blogspot.com/">Blog</a></li>
               </ul><!-- end navlist -->
            </div> <!-- end navcontainer -->
          
          <div class="social-media">
            <a target="_blank" class="icon fade" href="https://www.facebook.com"><img src="images/fb-logo.png"></a>
            <a target="_blank" class="icon fade" href="https://www.twitter.com"><img src="images/twitter-logo.png"></a>
            <a target="_blank" class="icon fade"href="https://www.youtube.com"><img src="images/youtube-logo.png"></a>
          </div>  <!-- end social-media -->
          </div> <!-- end container-fluid -->

          <div class="copyright-text">
            <p style="text-align: center">SURVEY SCRIBE 2015 | ALL RIGHTS RESERVED</p>
          </div> <!-- end copyright-text -->
        </footer> <!-- end footer -->
