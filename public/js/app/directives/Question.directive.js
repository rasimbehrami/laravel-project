angular
    .module("surveyscribe")
    .directive("questionDirective", function($compile){

	var radioTemplate = '<h6>{{question}}</h6><div class="q2">\
							   <form>\
							   	<div ng-repeat="answer in answers">\
							   	<input type="radio" name="answer" ng-model="answer.value" >\
							   	&nbsp;<h7>{{answer.description}}</h7>\
							   	</div>\
							   </form>\
						   </div>';

	var checkBoxTemplate = '<h6>{{question}}</h6><div class="q2">\
							   <form>\
							   	<div ng-repeat="answer in answers"><input type="checkbox" name="answer" ng-model="answer.value" >\
							   &nbsp;<h7>{{answer.description}}</h7>\
							   </div>\
							   </form>\
						   </div>';

	var optionsTemplate = '<h6>{{question}}</h6>\
							<div class="q2">\
							<select><option ng-repeat="answer in answers" value=""> {{answer.value}} </option>\
						   </select></div>';

	var messageTemplate = '<h6>{{question}}</h6>\
								<div class="q3">\
								<div class="message">\
								<textarea rows="4" style="max-width:28em;" class="form-control" id="text-q3" ng-model="answers[0].value"></textarea></div>\
         					</div>';

    var selectorTemplate = '<h6>{{question}}</h6>\
    						<div class="q4">\
					        <label for="fader">Drag the slider to change percentage.</label>\
					        <input type="range" min="0" max="100" ng-model="answers[0].value" id="fader" step="1" oninput="outputUpdate(value)">\
					          <output for="fader" id="volume">50</output>\
					          <script>\
					          function outputUpdate(vol) {\
					          document.querySelector("#volume").value = vol;}\
					          </script>\
        					</div>';

	var getTemplate = function(contentType){
		
		var template = '';

		switch(contentType){
			case 'radioTemplate':
			template = radioTemplate;
			break;

			case 'checkBoxTemplate':
			template = checkBoxTemplate;
			break;

			case 'optionsTemplate':
			template = optionsTemplate;
			break;

			case 'messageTemplate':
			template = messageTemplate;
			break;

			case 'selectorTemplate':
			template = selectorTemplate;
			break;
		}

		return template;
	};

	var linker = function(scope, element, attr){

 		element.html(getTemplate(scope.type)).show();

		console.log(scope.answers);

        scope.$watch('answers', function(newValue,oldValue){
        	console.log(newValue);
        },true);
       
        $compile(element.contents())(scope);

	};

	return {
		replace:true,
		scope:{
			type:'@',
			answers: '=', //Duhet = dhe jo @ sepse ne menyre qe te punoje ng-repeat brenda nje directive tjeter nevojitet 2-way data-binding
			question:'='
		},

		link: linker
	}
});
// .directive("childdir", function(){

// 	return{
// 		scope: {
// 			childscope:'='
// 		},
// 		link: function(scope, element, attrs){
// 			scope.$watch('childscope', function(newValue, oldValue){
// 				console.log(newValue);
// 			},true);
// 		},
// 		template: '</div>Child directive</div>'
// 	}
// });