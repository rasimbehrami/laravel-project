angular
    .module("surveyscribe")
    .controller("SurveyController", SurveyController);

function SurveyController($scope, SurveyService){

	$scope.dataFromServer = [];

	$scope.myData = [];

    $scope.postToServer = function(){
    	
  		$scope.questions = [
                        { 
                          "question":"1. What are some of the major problems that still exist in the third world countries?",
                          "viewType":"radioTemplate",
                            "answers": [
                                {
                                    "value": "Gezim",
                                    "description": "Answer option 1",
                                    "selected" : "false",
                                },
                                {
                                    "value": "Fabio",
                                    "description": "Answer option 2",
                                    "selected" : "false"
                                }
                            ]
                        },
                        {   
                          "question": "2.Check the Congress Candidate/s you think is more likely to win. ",
                            "viewType": "checkBoxTemplate",
                            "answers": [
                                {
                                    "value": "",
                                    "description": "John Conyers",
                                    "selected": "false"
                                },
                                {
                                    "value": "",
                                    "description": "Don Young",
                                    "selected": "false"
                                },
                                 {
                                    "value": "",
                                    "description": "Christopher Smith",
                                    "selected": "false"
                                },
                                 {
                                    "value": "",
                                    "description": "Joe Barton",
                                    "selected": "false"
                                }
                              ]
                        },
                        {   
                          "question": "3. Choose who you think best deserves this position? ",
                            "viewType": "optionsTemplate",
                            "answers": [
                                {
                                    "value": "John Conyers",
                                    "description": "",
                                    "selected": "true"
                                },
                                {
                                    "value": "Don Young",
                                    "description": "",
                                    "selected": "false"
                                },
                                 {
                                    "value": "Christopher Smith",
                                    "description": "",
                                    "selected": "false"
                                },
                                 {
                                    "value": "Joe Barton",
                                    "description": "",
                                    "selected": "false"
                                }
                              ]
                        },
                        {   
                          "question": "4. What is your opinion on the politics of the president of the United States?: ",
                            "viewType": "messageTemplate",
                            "answers": [
                                {
                                    "value": "Write your answer here",
                                    "description": "Answer option 1",
                                    "selected": "true"
                                }
                              ]
                        },
                        {   
                          "question": "5. At what percentage has Barack Obama fulfilled his promises? ",
                            "viewType": "selectorTemplate",
                            "answers": [
                                {
                                    "value": "64",
                                    "description": "",
                                    "selected": "true"
                                }
                              ]
                        }
                    ];
          };
};


          // $scope.ResponseFromServer = SurveyService.getAllSurveys().then(function(data){
      //       console.log(data.data);
      //       $scope.dataFromServer = [];

      //     $.each(data.data, function(index, value){
      //       $scope.dataFromServer.push(value);
      //     });

      //         });
      //   };


      // $scope.ResponseFromServer = SurveyService.addSurvey('1','Ckemi').then(function(data){
  //        console.log(data.data);
  //        $scope.dataFromServer = [];

  //        // $.each(data.data, function(index, value){
  //        //  $scope.dataFromServer.push(value.title);
  //        // })
  //      });