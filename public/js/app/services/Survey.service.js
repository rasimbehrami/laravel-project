angular
    .module("surveyscribe")
    .factory("SurveyService",SurveyService);

function SurveyService($http){

	var addSurvey = function(userId, surveyTemplate){
		return $http.post('/survey',{"userId":userId,"surveyTemplate":surveyTemplate});
	};

	var removeSurvey = function(surveyId){
		return $http.delete('/survey/' + surveyId);
	};

	var getAllSurveys = function(){
		return $http.get('/survey');	
	};

    var getSurvey = function(surveyId){
        return $http.get('/survey/'+ surveyId);
    };

    var surveys = getAllSurveys();

	return{
		addSurvey: addSurvey,
		removeSurvey: removeSurvey,
		getAllSurveys: getAllSurveys,
		surveys: surveys,
        getSurvey: getSurvey
	}
}